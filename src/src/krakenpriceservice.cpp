#include "krakenpriceservice.h"

#include <QJsonObject>
#include <QJsonArray>

const QUrl KrakenPriceService::TICKER_URL = QUrl("https://api.kraken.com/0/public/Ticker?pair=XBTEUR,XBTUSD");

KrakenPriceService::KrakenPriceService(QObject *parent) :
    NetworkPriceService(parent),
    _latestDoc()
{
    setAvailableCurrencies(QStringList() << "EUR" << "USD");
}

QUrl KrakenPriceService::getUrl() const
{
    return TICKER_URL;
}

bool KrakenPriceService::requestReceived(QByteArray body)
{
    setLatestDoc(body);
    return isLatestDocValid();
}

Price KrakenPriceService::getLatestPrice() const
{
    if (isLatestDocValid()) {
        return readPriceFromLatestDoc();
    }
    return NULL_PRICE;
}

void KrakenPriceService::setLatestDoc(QByteArray val)
{
    QJsonDocument doc = QJsonDocument::fromJson(val);
    if (doc.isObject()) {
        _latestDoc.setObject(doc.object());
    }
}

bool KrakenPriceService::isLatestDocValid() const {
    return !_latestDoc.isEmpty(); // ?
}

Price KrakenPriceService::readPriceFromLatestDoc() const {
    QJsonObject root = _latestDoc.object();
    QJsonValue result = root["result"];
    if (!result.isObject()) {
        return NULL_PRICE;
    }

    QJsonValue val  = result.toObject()["XXBTZ"+currency()];
    if (!val.isObject()) {
        return NULL_PRICE;
    }

    QJsonObject co = val.toObject();

    double last = toDouble(co["c"].toArray()[0].toString().toDouble());
    double bid = toDouble(co["b"].toArray()[0].toString().toDouble());
    double ask = toDouble(co["a"].toArray()[0].toString().toDouble());
    double avg = toDouble(co["p"].toArray()[1].toString().toDouble());

    return Price(currency(), last, bid, ask, avg);
}

double KrakenPriceService::toDouble(QJsonValue val)
{
    return val.isDouble() ? val.toDouble() : -1.0;
}
