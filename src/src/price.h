#ifndef PRICE_H
#define PRICE_H

#include <QString>
#include <QDebug>

class Price
{
public:
    Price() :
        _currency(""),
        _last(-1.0),
        _buy(-1.0),
        _sell(-1.0),
        _avg24h(-1.0)
    {}

    Price(QString currency, double last, double buy, double sell, double avg24h) :
        _currency(currency),
        _last(last),
        _buy(buy),
        _sell(sell),
        _avg24h(avg24h)
    {}

    QString currency() const { return _currency; }
    double last() const { return _last; }
    double buy() const { return _buy; }
    double sell() const { return _sell; }
    double avg24h() const { return _avg24h; }

    bool operator==(const Price& other) const {
        return other.currency()==currency() &&
                other.last()==last() &&
                other.buy()==buy() &&
                other.sell()==sell() &&
                other.avg24h()==avg24h();
    }

    bool operator!=(const Price& other) const {
        return !((*this)!=other);
    }

    bool isNull() {
        return last() < 0; // ?
    }

private:

    QString _currency;
    double _last;
    double _buy;
    double _sell;
    double _avg24h;
};

const Price NULL_PRICE;

#endif // PRICE_H
