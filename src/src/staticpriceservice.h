#ifndef STATICPRICESERVICE_H
#define STATICPRICESERVICE_H

#include "priceservice.h"
#include "price.h"

class StaticPriceService : public PriceService
{
    Q_OBJECT
public:
    explicit StaticPriceService(Price price, QObject *parent = 0) :
        PriceService(parent),
        _price(price)
    {
    }

    Q_INVOKABLE virtual void refresh() {}

    virtual QString currency() const { return _price.currency(); }
    virtual void setCurrency(QString) {}

    virtual QStringList availableCurrencies() const { return QStringList() << _price.currency(); }

    virtual double priceLast() const { return _price.last(); }
    virtual double priceBuy() const { return _price.buy(); }
    virtual double priceSell() const { return _price.sell(); }
    virtual double priceAvg24h() const { return _price.avg24h(); }

    virtual QString statusText() const { return "Static price"; }

    virtual bool refreshing() const { return false; }

    virtual QString error() const { return ""; }

private:
    Price _price;

};

#endif // STATICPRICESERVICE_H
