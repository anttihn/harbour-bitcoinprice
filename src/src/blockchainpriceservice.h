#ifndef BLOCKCHAINPRICESERVICE_H
#define BLOCKCHAINPRICESERVICE_H



#include <QJsonDocument>

#include "networkpriceservice.h"



class BlockchainPriceService : public NetworkPriceService
{
    Q_OBJECT
public:
    explicit BlockchainPriceService(QObject *parent = 0);

protected:

    virtual QUrl getUrl() const;
    virtual bool requestReceived(QByteArray body);
    virtual Price getLatestPrice() const;


private:

    static const QUrl BLOCKCHAIN_URL;

    void setLatestDoc(QByteArray val);

    Price readPriceFromLatestDoc() const; // TODO name
    void readCurrenciesFromLatestDoc();

    bool isLatestDocValid() const;

    static double toDouble(QJsonValue val);

    QJsonDocument _latestDoc;

};

#endif // BLOCKCHAINPRICESERVICE_H
