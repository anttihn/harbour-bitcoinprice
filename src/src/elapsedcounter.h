#ifndef ELAPSEDCOUNTER_H
#define ELAPSEDCOUNTER_H

#include <QObject>
#include <QTimer>
#include <QDateTime>
#include <QVector>

/**
 * Tells the elapsed time as a human readable localized string by
 * emitting an elapsed() signal once in a while.
 */
class ElapsedCounter : public QObject
{
    Q_OBJECT
public:
    explicit ElapsedCounter(QObject *parent = 0);

    virtual ~ElapsedCounter();

    void restart();

    void stop();

signals:

    void elapsed(QString elapsed);

private slots:

    void timeElapsed();

private:
    ElapsedCounter operator=(const ElapsedCounter& other);

    static QString elapsedStringFromSeconds(int seconds);

    int nextDelay();

    const static QString ELAPSED_LITTLE;

    const static QVector<int> ELAPSED_DELAYS;
    static QVector<int> initELAPSED_DELAYS();

    int _elapsedIndex;

    QTimer _timer;

    QDateTime _latestUpdate;

};

#endif // ELAPSEDCOUNTER_H
