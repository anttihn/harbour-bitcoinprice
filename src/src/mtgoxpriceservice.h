#ifndef MTGOXPRICESERVICE_H
#define MTGOXPRICESERVICE_H

#include <QJsonDocument>

#include "networkpriceservice.h"

class MtgoxPriceService : public NetworkPriceService
{
    Q_OBJECT
public:
    explicit MtgoxPriceService(QObject *parent = 0);

protected:

    virtual QUrl getUrl() const;
    virtual bool requestReceived(QByteArray body);
    virtual Price getLatestPrice() const;


private:

    static const QString TICKER_URL;

    void setLatestDoc(QByteArray val);

    Price readPriceFromLatestDoc() const; // TODO name

    bool isLatestDocValid() const;

    static double toDouble(QJsonValue val);

    QJsonDocument _latestDoc;

};

#endif // MTGOXPRICESERVICE_H
