#include "elapsedcounter.h"

#include <QString>

const QString ElapsedCounter::ELAPSED_LITTLE = QObject::tr("Updated just now", "Very recently, less than 15 seconds ago or so.");

const QVector<int> ElapsedCounter::ELAPSED_DELAYS = initELAPSED_DELAYS();


ElapsedCounter::ElapsedCounter(QObject *parent) :
  QObject(parent),
  _elapsedIndex(0),
  _timer(this),
  _latestUpdate()
{
  _timer.setSingleShot(true);
  connect(&_timer, SIGNAL(timeout()), this, SLOT(timeElapsed()));
}

ElapsedCounter::~ElapsedCounter()
{
    // This probably isn't needed (?) but doesn't do any harm either.
    _timer.stop();
}

void ElapsedCounter::restart()
{
    _elapsedIndex = 0;
    _latestUpdate = QDateTime::currentDateTime();
    _timer.stop();
    _timer.start(nextDelay());
    emit elapsed(ELAPSED_LITTLE);
}

void ElapsedCounter::stop()
{
    _timer.stop();
}

void ElapsedCounter::timeElapsed()
{
    int secs = _latestUpdate.secsTo(QDateTime::currentDateTime());
    emit elapsed(elapsedStringFromSeconds(secs));
    _timer.start(nextDelay());
}


QString ElapsedCounter::elapsedStringFromSeconds(int seconds)
{
    int n;
    if (seconds < 15) {
        return ELAPSED_LITTLE;
    }
    else if (seconds < 55) {
        n = seconds - (seconds%10);
        return n == 0 ?
                    tr("Updated seconds ago") :
                    tr("Updated %1 seconds ago", 0, n).arg(n);
    }
    else if (seconds < 60*55) {
        n = (seconds+30) / 60;
        if (n > 10) {
            n = n - (n%5);
        }
        return n == 1 ?
                    tr("Updated a minute ago") :
                    tr("Updated %1 minutes ago", 0, n).arg(n);

    }
    else if (seconds < 60*60*24) {
        n = (seconds+60*30) / (60*60);
        return n == 1 ?
                    tr("Updated an hour ago") :
                    tr("Updated %1 hours ago", 0, n).arg(n);
    }
    else {
        n = (seconds+60*60*12) / (60*60*24);
        return tr("Updated %1 days ago", 0, n).arg(n);
    }
}

int ElapsedCounter::nextDelay()
{
    int secs = _elapsedIndex < ELAPSED_DELAYS.size() ? ELAPSED_DELAYS[_elapsedIndex++] : 60*60*24;
    return secs * 1000;
}

QVector<int> ElapsedCounter::initELAPSED_DELAYS() {
    QVector<int> x;
    for (int i=0; i<5; ++i) { // 1,2,...,5 minutes
        x.push_back(60);
    }
    for (int i=0; i<5; ++i) { // 10,15,...,30 minutes
        x.push_back(60*5);
    }
    for (int i=0; i<3; ++i) { // 40,50,60 minutes
        x.push_back(60*10);
    }
    for (int i=0; i<23; ++i) { // 2,3,...24 hours
        x.push_back(60*60);
    }
    return x;
}
