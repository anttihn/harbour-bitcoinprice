#ifndef MULTISOURCEPRICESERVICE_H
#define MULTISOURCEPRICESERVICE_H

#include <QSettings>

#include "price.h"
#include "priceservice.h"

class MultiSourcePriceService : public PriceService
{
    Q_OBJECT

    Q_PROPERTY(QString source READ source WRITE setSource NOTIFY sourceChanged)
    Q_PROPERTY(QStringList availableSources READ availableSources NOTIFY availableSourcesChanged)

    Q_PROPERTY(QStringList shortcuts READ shortcuts NOTIFY shortcutsChanged)

public:

    enum PriceSource {
        BLOCKCHAIN_INFO = 0,
        KRAKEN,
        MTGOX,
        NONE
    };

    explicit MultiSourcePriceService(QObject *parent = 0);

    Q_INVOKABLE virtual void refresh() { _current->refresh(); }

    Q_INVOKABLE void pushShortcut();

    Q_INVOKABLE void clearShortcuts();

    virtual QString currency() const { return _current->currency(); }
    virtual void setCurrency(QString currency) { _current->setCurrency(currency); }

    virtual QStringList availableCurrencies() const { return _current->availableCurrencies(); }

    virtual double priceLast() const { return _current->priceLast(); }
    virtual double priceBuy() const { return _current->priceBuy(); }
    virtual double priceSell() const { return _current->priceSell(); }
    virtual double priceAvg24h() const { return _current->priceAvg24h(); }

    virtual QString statusText() const { return _current->statusText(); }
    virtual bool refreshing() const { return _current->refreshing(); }
    virtual QString error() const { return _current->error(); }

    QString source() const;
    void setSource(QString name);

    QStringList availableSources() const;

    QStringList shortcuts();

signals:
    void sourceChanged();
    void availableSourcesChanged();
    void shortcutsChanged();

public slots:
    void saveSettings();

private:

    static PriceSource sourceForName(QString name);
    static QString nameForSource(PriceSource source);

    PriceService* serviceForSource(PriceSource name);

    void setCurrentService(PriceService* service);

    void loadShortcutsFromSettings();

    void loadDefaultFromSettings();

    QString currentAsShortcut() const;

    PriceSource _currentSource;
    PriceService* _current;

    QStringList _shortcuts;
    QString _currentShortcut;
};

#endif // MULTISOURCEPRICESERVICE_H
