#ifndef BITCOINPRICESERVICE_H
#define BITCOINPRICESERVICE_H


#include <QUrl>
#include <QNetworkReply>

#include <QByteArray>

#include "price.h"
#include "elapsedcounter.h"

#include "priceservice.h"

/**
 * An interface to the Bitcoin price data provided by blockchain.info.
 *
 * Usable from QML.
 */
class NetworkPriceService : public PriceService
{
    Q_OBJECT


public:
    explicit NetworkPriceService(QObject *parent = 0);

    virtual ~NetworkPriceService();

    Q_INVOKABLE virtual void refresh();

    virtual QString currency() const;
    virtual void setCurrency(QString currency);

    virtual QStringList availableCurrencies() const;

    virtual double priceLast() const;
    virtual double priceBuy() const;
    virtual double priceSell() const;
    virtual double priceAvg24h() const;

    virtual QString statusText() const;

    virtual bool refreshing() const;

    virtual QString error() const;

protected:



    virtual QUrl getUrl() const = 0;

    virtual bool requestReceived(QByteArray body) = 0;

    virtual Price getLatestPrice() const = 0;

    void setAvailableCurrencies(const QStringList& currencies);

private slots:
    void requestFinished(QNetworkReply *reply);
    void setStatusText(QString text);

private:
    void setPrice(const Price& price);
    void setError(QString error);


    QString _currency;
    QStringList _currencies;

    Price _price;

    QNetworkAccessManager _accessManager;

    int _refreshing;

    ElapsedCounter _elapsedCounter;

    QString _statusText;

    QString _error;


};

#endif // BITCOINPRICESERVICE_H
