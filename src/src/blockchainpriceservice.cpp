#include "blockchainpriceservice.h"

#include <QJsonObject>

const QUrl BlockchainPriceService::BLOCKCHAIN_URL = QUrl("https://blockchain.info/ticker");

BlockchainPriceService::BlockchainPriceService(QObject *parent) :
    NetworkPriceService(parent),
    _latestDoc()
{
    setAvailableCurrencies(QStringList() <<
                           "AUD" << "CAD" << "CHF" << "CNY" << "DKK" << "EUR" <<
                           "GBP" << "HKD" << "JPY" << "NZD" << "PLN" << "RUB" <<
                           "SEK" << "SGD" << "THB" << "USD");
}

QUrl BlockchainPriceService::getUrl() const
{
    return BLOCKCHAIN_URL;
}

bool BlockchainPriceService::requestReceived(QByteArray body)
{
    setLatestDoc(body);
    if (isLatestDocValid()) {
        readCurrenciesFromLatestDoc();
        return true;
    }
    return false;
}

Price BlockchainPriceService::getLatestPrice() const
{
    if (isLatestDocValid()) {
        return readPriceFromLatestDoc();
    }
    return NULL_PRICE;
}

void BlockchainPriceService::setLatestDoc(QByteArray val)
{
    QJsonDocument doc = QJsonDocument::fromJson(val);
    if (doc.isObject()) {
        _latestDoc.setObject(doc.object());
    }
}

bool BlockchainPriceService::isLatestDocValid() const {
    return !_latestDoc.isEmpty(); // ?
}

void BlockchainPriceService::readCurrenciesFromLatestDoc() {
    QJsonObject root = _latestDoc.object();
    QStringList keys = root.keys();
    QStringList currencies;
    for (QStringList::const_iterator it = keys.begin(); it != keys.end(); ++it) {
        currencies.push_back(*it);
    }
    setAvailableCurrencies(currencies);
}

Price BlockchainPriceService::readPriceFromLatestDoc() const {
    QJsonObject root = _latestDoc.object();
    QJsonValue cv = root[currency()];
    if (!cv.isObject()) {
        return NULL_PRICE;
    }

    QJsonObject co = cv.toObject();

    return Price(currency(),
            toDouble(co["last"]),
            toDouble(co["buy"]),
            toDouble(co["sell"]),
            toDouble(co["24h"]));
}

double BlockchainPriceService::toDouble(QJsonValue val)
{
    return val.isDouble() ? val.toDouble() : -1.0;
}


