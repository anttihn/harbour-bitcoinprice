#include "networkpriceservice.h"

#include <QSettings>
#include <QtNetwork>
#include <QNetworkAccessManager>


NetworkPriceService::NetworkPriceService(QObject *parent) :
    PriceService(parent),
    _currency(),
    _currencies(),
    _price(),
    _accessManager(this),
    _refreshing(0),
    _elapsedCounter(this),
    _statusText(tr("No data")),
    _error("")

{
    connect(&_accessManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(requestFinished(QNetworkReply*)));
    connect(&_elapsedCounter, SIGNAL(elapsed(QString)), this, SLOT(setStatusText(QString)));
}

NetworkPriceService::~NetworkPriceService()
{

}

void NetworkPriceService::refresh()
{
    // Sending a new request even if another is still going?
    // TODO: is that the right thing to do?

    _accessManager.get(QNetworkRequest(getUrl()));
    _refreshing++;
    emit refreshingChanged();
}

QString NetworkPriceService::currency() const
{
    return _currency;
}

void NetworkPriceService::setCurrency(QString currency)
{
    if (_currency==currency) {
        return;
    }

    _currency = currency;

    emit currencyChanged();

    setPrice(getLatestPrice());
}

QStringList NetworkPriceService::availableCurrencies() const
{
    return _currencies;
}

double NetworkPriceService::priceLast() const
{
    return _price.last();
}
double NetworkPriceService::priceBuy() const
{
    return _price.buy();
}
double NetworkPriceService::priceSell() const
{
    return _price.sell();
}
double NetworkPriceService::priceAvg24h() const
{
    return _price.avg24h();
}

QString NetworkPriceService::statusText() const
{
    return _statusText;
}

bool NetworkPriceService::refreshing() const
{
    return _refreshing > 0;
}

QString NetworkPriceService::error() const
{
    return _error;
}

void NetworkPriceService::requestFinished(QNetworkReply* reply)
{
    bool success = reply->error() == QNetworkReply::NoError &&
            reply->isReadable() &&
            requestReceived(reply->readAll());

    if (success) {
        setError("");
        setPrice(getLatestPrice());
        _elapsedCounter.restart();
    }
    else {
        setError(tr("Network error"));
    }

    reply->deleteLater();

    _refreshing--;

    emit refreshingChanged();
}

void NetworkPriceService::setStatusText(QString text)
{
    _statusText = text;

    emit statusTextChanged();
}

void NetworkPriceService::setPrice(const Price &price)
{
    Price oldPrice(_price);
    _price = price;

    if (oldPrice.last()!=price.last()) {
        emit priceLastChanged();
    }
    if (oldPrice.buy()!=price.buy()) {
        emit priceBuyChanged();
    }
    if (oldPrice.sell()!=price.sell()) {
        emit priceSellChanged();
    }
    if (oldPrice.avg24h()!=price.avg24h()) {
        emit priceAvg24hChanged();
    }
}

void NetworkPriceService::setAvailableCurrencies(const QStringList &currencies)
{
    if (currencies!=_currencies) {
        _currencies = currencies;
        emit availableCurrenciesChanged();
    }
}

void NetworkPriceService::setError(QString error)
{
    if (error!=_error) {
        _error = error;
        emit errorChanged();
    }
}



