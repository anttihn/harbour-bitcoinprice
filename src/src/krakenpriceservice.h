#ifndef KRAKENPRICESERVICE_H
#define KRAKENPRICESERVICE_H

#include <QJsonDocument>

#include "networkpriceservice.h"

class KrakenPriceService : public NetworkPriceService
{
    Q_OBJECT
public:
    explicit KrakenPriceService(QObject *parent = 0);

protected:

    virtual QUrl getUrl() const;
    virtual bool requestReceived(QByteArray body);
    virtual Price getLatestPrice() const;


private:

    static const QUrl TICKER_URL;

    void setLatestDoc(QByteArray val);

    Price readPriceFromLatestDoc() const; // TODO name

    bool isLatestDocValid() const;

    static double toDouble(QJsonValue val);

    QJsonDocument _latestDoc;

};

#endif // KRAKENPRICESERVICE_H
