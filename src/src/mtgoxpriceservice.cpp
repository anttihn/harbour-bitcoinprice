#include "mtgoxpriceservice.h"

#include <QJsonObject>
#include <QJsonArray>

const QString MtgoxPriceService::TICKER_URL = QString("http://data.mtgox.com/api/2/BTC%1/money/ticker_fast");

MtgoxPriceService::MtgoxPriceService(QObject *parent) :
    NetworkPriceService(parent),
    _latestDoc()
{
    setAvailableCurrencies(QStringList() << "EUR" << "USD");
}

QUrl MtgoxPriceService::getUrl() const
{
    return QUrl(TICKER_URL.arg(currency()));
}

bool MtgoxPriceService::requestReceived(QByteArray body)
{
    setLatestDoc(body);
    return isLatestDocValid();
}

Price MtgoxPriceService::getLatestPrice() const
{
    if (isLatestDocValid()) {
        return readPriceFromLatestDoc();
    }
    return NULL_PRICE;
}

void MtgoxPriceService::setLatestDoc(QByteArray val)
{
    QJsonDocument doc = QJsonDocument::fromJson(val);
    if (doc.isObject()) {
        _latestDoc.setObject(doc.object());
    }
}

bool MtgoxPriceService::isLatestDocValid() const {
    return !_latestDoc.isEmpty(); // ?
}

Price MtgoxPriceService::readPriceFromLatestDoc() const {
    QJsonObject root = _latestDoc.object();
    QJsonValue result = root["data"];
    if (!result.isObject()) {
        return NULL_PRICE;
    }

    QString curr = result.toObject()["last"].toObject()["currency"].toString();
    if (curr!=currency()) {
        return NULL_PRICE;
    }

    double last  = toDouble(result.toObject()["last"].toObject()["value"].toString().toDouble());
    double buy  = toDouble(result.toObject()["buy"].toObject()["value"].toString().toDouble());
    double sell  = toDouble(result.toObject()["sell"].toObject()["value"].toString().toDouble());

    return Price(currency(), last, buy, sell, -1);
}

double MtgoxPriceService::toDouble(QJsonValue val)
{
    return val.isDouble() ? val.toDouble() : -1.0;
}
