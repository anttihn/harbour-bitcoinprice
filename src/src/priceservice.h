#ifndef PRICESERVICE_H
#define PRICESERVICE_H

#include <QObject>
#include <QStringList>

#include "price.h"

class PriceService : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString currency READ currency WRITE setCurrency NOTIFY currencyChanged)
    Q_PROPERTY(QStringList availableCurrencies READ availableCurrencies NOTIFY availableCurrenciesChanged)

    Q_PROPERTY(double priceLast READ priceLast NOTIFY priceLastChanged)
    Q_PROPERTY(double priceBuy READ priceBuy NOTIFY priceBuyChanged)
    Q_PROPERTY(double priceSell READ priceSell NOTIFY priceSellChanged)
    Q_PROPERTY(double priceAvg24h READ priceAvg24h NOTIFY priceAvg24hChanged)

    Q_PROPERTY(QString statusText READ statusText NOTIFY statusTextChanged)
    Q_PROPERTY(bool refreshing READ refreshing NOTIFY refreshingChanged)

    Q_PROPERTY(QString error READ error NOTIFY errorChanged)
public:
    explicit PriceService(QObject *parent = 0) :
        QObject(parent)
    {}

    Q_INVOKABLE virtual void refresh() = 0;

    virtual QString currency() const = 0;
    virtual void setCurrency(QString currency) = 0;

    virtual QStringList availableCurrencies() const = 0;

    virtual double priceLast() const = 0;
    virtual double priceBuy() const = 0;
    virtual double priceSell() const = 0;
    virtual double priceAvg24h() const = 0;

    virtual QString statusText() const = 0;

    virtual bool refreshing() const = 0;

    virtual QString error() const = 0;

signals:
    void currencyChanged();
    void availableCurrenciesChanged();

    void priceLastChanged();
    void priceBuyChanged();
    void priceSellChanged();
    void priceAvg24hChanged();

    void statusTextChanged();
    void refreshingChanged();

    void errorChanged();

};



#endif // PRICESERVICE_H
