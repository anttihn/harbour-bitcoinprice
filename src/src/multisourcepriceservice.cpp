#include "multisourcepriceservice.h"

#include "blockchainpriceservice.h"
#include "krakenpriceservice.h"
#include "mtgoxpriceservice.h"

static QString NAME_BLOCKCHAININFO("Blockchain.info");
static QString NAME_KRAKEN("Kraken");
static QString NAME_MTGOX("MtGox");

MultiSourcePriceService::MultiSourcePriceService(QObject *parent) :
    PriceService(parent),
    _currentSource(NONE),
    _current(0),
    _shortcuts(),
    _currentShortcut()
{
    loadShortcutsFromSettings();
    loadDefaultFromSettings();
}

void MultiSourcePriceService::pushShortcut()
{
    QString curr = currentAsShortcut();
    if (curr==_currentShortcut) {
        return;
    }

    if (!_currentShortcut.isEmpty()) {
        bool removed = _shortcuts.removeOne(curr);
        if (!removed && _shortcuts.size() == 3) {
            _shortcuts.pop_front();
        }
        _shortcuts.push_back(_currentShortcut);
    }

    _currentShortcut = curr;

    emit shortcutsChanged();
}

void MultiSourcePriceService::clearShortcuts()
{
    _currentShortcut.clear();
    if (!_shortcuts.isEmpty()) {
        _shortcuts.clear();
        emit shortcutsChanged();
    }
}

QString MultiSourcePriceService::source() const
{
    return nameForSource(_currentSource);
}

void MultiSourcePriceService::setSource(QString name)
{
    PriceSource source = sourceForName(name);

    if (source==NONE) {
        qDebug() << "ERROR: Invalid source: " << name;
        return;
    }

    if (_currentSource==source) {
        return;
    }

    _currentSource = source;
    setCurrentService(serviceForSource(_currentSource));
    emit sourceChanged();
}

QStringList MultiSourcePriceService::availableSources() const {
    return QStringList() << NAME_BLOCKCHAININFO << NAME_KRAKEN << NAME_MTGOX;
}

QStringList MultiSourcePriceService::shortcuts()
{
    return _shortcuts;
}

void MultiSourcePriceService::saveSettings()
{
    if (_currentShortcut.isEmpty()) {
        QSettings().remove("default");
    }
    else {
        QSettings().setValue("default", _currentShortcut);
    }

    QSettings().setValue("shortcuts", _shortcuts);
}

MultiSourcePriceService::PriceSource MultiSourcePriceService::sourceForName(QString name)
{
    if (NAME_BLOCKCHAININFO==name) return BLOCKCHAIN_INFO;
    if (NAME_KRAKEN==name) return KRAKEN;
    if (NAME_MTGOX==name) return MTGOX;
    return NONE;
}

QString MultiSourcePriceService::nameForSource(MultiSourcePriceService::PriceSource source)
{
    switch(source) {
    case BLOCKCHAIN_INFO: return NAME_BLOCKCHAININFO;
    case KRAKEN: return NAME_KRAKEN;
    case MTGOX: return NAME_MTGOX;
    default: Q_ASSERT(false); return "";
    }
}

PriceService *MultiSourcePriceService::serviceForSource(MultiSourcePriceService::PriceSource name)
{
    if (name==BLOCKCHAIN_INFO) {
        return new BlockchainPriceService(this);
    }
    if (name==KRAKEN) {
        return new KrakenPriceService(this);
    }
    if (name==MTGOX) {
        return new MtgoxPriceService(this);
    } 
    Q_ASSERT(false);
    return 0;
}

void MultiSourcePriceService::setCurrentService(PriceService *service)
{
    QString curre;
    if (_current) {
        curre = currency();
        delete _current;
    }

    _current = service;
    _current->setCurrency(curre);

    connect(_current, SIGNAL(priceLastChanged()), this, SIGNAL(priceLastChanged()));
    connect(_current, SIGNAL(priceBuyChanged()), this, SIGNAL(priceBuyChanged()));
    connect(_current, SIGNAL(priceSellChanged()), this, SIGNAL(priceSellChanged()));
    connect(_current, SIGNAL(priceAvg24hChanged()), this, SIGNAL(priceAvg24hChanged()));

    connect(_current, SIGNAL(currencyChanged()), this, SIGNAL(currencyChanged()));
    connect(_current, SIGNAL(availableCurrenciesChanged()), this, SIGNAL(availableCurrenciesChanged()));

    connect(_current, SIGNAL(statusTextChanged()), this, SIGNAL(statusTextChanged()));
    connect(_current, SIGNAL(errorChanged()), this, SIGNAL(errorChanged()));
    connect(_current, SIGNAL(refreshingChanged()), this, SIGNAL(refreshingChanged()));

    emit priceLastChanged();
    emit priceBuyChanged();
    emit priceSellChanged();
    emit priceAvg24hChanged();

    emit currencyChanged();
    emit availableCurrenciesChanged();

    emit statusTextChanged();
    emit errorChanged();
}

void MultiSourcePriceService::loadDefaultFromSettings() {
    _currentShortcut = QSettings().value("default", "USD:"+NAME_MTGOX).toString();
    QStringList parts = _currentShortcut.split(":");
    if (sourceForName(parts.at(1))==NONE) {
        // There was some invalid stuff in settings.
        // Reverting to default.
        _currentShortcut = "USD:"+NAME_MTGOX;
        parts = QStringList() << "USD" << NAME_MTGOX;
    }
    setSource(parts.at(1));
    setCurrency(parts.at(0));
}

QString MultiSourcePriceService::currentAsShortcut() const
{
    return _current->currency() + ":" + nameForSource(_currentSource);
}


void MultiSourcePriceService::loadShortcutsFromSettings()
{
    QStringList shorts = QSettings().value("shortcuts").toStringList();
    if (shorts!=_shortcuts) {
        _shortcuts = shorts;
        emit shortcutsChanged();
    }
}

