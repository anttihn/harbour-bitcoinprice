import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    property string currency

    SilicaFlickable {
        anchors.fill: parent

        contentHeight: parent.height

        Column {
            id: column
            anchors.fill: parent


            spacing: Theme.paddingLarge

            PageHeader {
                title: qsTr("Options", "title");
            }

            ComboBox {
                id: sourceBox
                x: Theme.paddingLarge
                label: qsTr("Source")

                currentIndex: bitcoinprice.availableSources.indexOf(bitcoinprice.source)

                menu: ContextMenu {
                    Repeater {
                        model: bitcoinprice.availableSources
                        MenuItem { text: modelData }
                    }
                }

                onCurrentItemChanged: {
                    if (currentItem!==null) {
                        bitcoinprice.source = currentItem.text;
                    }
                }
            }

            ComboBox {
                id: currencyBox
                x: Theme.paddingLarge
                label: qsTr("Currency")

                currentIndex: bitcoinprice.availableCurrencies.indexOf(bitcoinprice.currency)

                menu: ContextMenu {
                    Repeater {
                        id: currencyRepeater
                        model: bitcoinprice.availableCurrencies
                        MenuItem { text: modelData }
                    }
                }

                onCurrentItemChanged: {
                    if (currentItem!==null) {
                        bitcoinprice.currency = currentItem.text;
                    }
                }
            }

            Button {
                x: Theme.paddingLarge // ?
                text: qsTr("Clear shortcuts")
                onClicked: {
                    bitcoinprice.clearShortcuts();
                }
            }
        }
    }
}
