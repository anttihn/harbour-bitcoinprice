/*
  Copyright (C) 2013 Jolla Ltd.
  Contact: Thomas Perl <thomas.perl@jollamobile.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0

import "../components"

Page {
    id: page

    property var shortcuts: []

    SilicaFlickable {
        anchors.fill: parent

        contentHeight: parent.height


        PullDownMenu {

            MenuItem {
                id: settingsItem
                text: qsTr("Options")
                onClicked: pageStack.push(Qt.resolvedUrl("OptionsPage.qml"))
            }

            Repeater {
                model: bitcoinprice.shortcuts


                MenuItem {
                    function prettyPrint(data) {
                        var parts = data.split(":", 2);
                        return qsTr("%1 %2").arg(parts[1]).arg(parts[0]);
                    }
                    text: prettyPrint(modelData)
                    font.pixelSize: Theme.fontSizeSmall
                    onClicked: {
                        var parts = modelData.split(":");
                        bitcoinprice.source = parts[1];
                        bitcoinprice.currency = parts[0];
                        bitcoinprice.refresh();
                    }
                }
            }

            onActiveChanged: {
                if (!active) {
                    bitcoinprice.pushShortcut();
                }
            }
        }

        Column {
            id: column
            anchors.fill: parent

            spacing: Theme.paddingLarge

            PageHeader {
                title: qsTr("1 BTC in %1").arg(bitcoinprice.source)
            }

            PriceLabel {
                text: qsTr("Last")
                price: bitcoinprice.priceLast
                currency: bitcoinprice.currency
                loading: bitcoinprice.refreshing
                x: Theme.paddingLarge
            }
            PriceLabel {
                text: qsTr("Buy")
                price: bitcoinprice.priceBuy
                currency: bitcoinprice.currency
                loading: bitcoinprice.refreshing
                x: Theme.paddingLarge
            }
            PriceLabel {
                text: qsTr("Sell")
                price: bitcoinprice.priceSell
                currency: bitcoinprice.currency
                loading: bitcoinprice.refreshing
                x: Theme.paddingLarge
            }
            PriceLabel {
                text: qsTr("24h average")
                currency: bitcoinprice.currency
                price: bitcoinprice.priceAvg24h
                loading: bitcoinprice.refreshing
                x: Theme.paddingLarge
            }
        }

        Label {
            id: errorLabel
            visible: bitcoinprice.error
            anchors.margins: Theme.paddingMedium
            anchors.bottom: refreshButton.top
            anchors.horizontalCenter: parent.horizontalCenter
            text: bitcoinprice.error
            color: Theme.highlightColor
            font.pixelSize: Theme.fontSizeMedium
        }

        IconButton {
            id: refreshButton
            anchors.bottom: statusLabel.top
            anchors.horizontalCenter: parent.horizontalCenter
            icon.source: "image://theme/icon-m-refresh"
            onClicked: {
                bitcoinprice.refresh();
            }
        }

        Label {
            id: statusLabel
            anchors.margins: Theme.paddingMedium
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            text: bitcoinprice.refreshing ? qsTr("Updating...") : bitcoinprice.statusText
            color: Theme.secondaryColor
            font.pixelSize: Theme.fontSizeSmall
        }
    }

    onStatusChanged: {
        if (status===PageStatus.Active) {
            bitcoinprice.pushShortcut();
            bitcoinprice.refresh();
        }
    }

}


