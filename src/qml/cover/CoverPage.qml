/*
  Copyright (C) 2013 Jolla Ltd.
  Contact: Thomas Perl <thomas.perl@jollamobile.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {

    Label {
        id: btcLabel
        y: Theme.paddingLarge
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width - Theme.paddingSmall*2
        wrapMode: Text.Wrap
        text: qsTr("1 BTC");
        font.pixelSize: Theme.fontSizeMedium
        horizontalAlignment: Text.AlignHCenter
    }

    Label {
        id: equalsLabel
        anchors.top: btcLabel.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width - Theme.paddingSmall*2
        text: qsTr("\u2248");
        font.pixelSize: Theme.fontSizeMedium
        horizontalAlignment: Text.AlignHCenter
    }

    Label {
        id: currencyLabel
        anchors.top: equalsLabel.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width - Theme.paddingSmall*2
        wrapMode: Text.Wrap
        color: Theme.primaryColor
        text: bitcoinprice.priceLast < 0 ? qsTr("?") : qsTr("%L1 %2", "value currency").arg(bitcoinprice.priceLast.toFixed(0)).arg(bitcoinprice.currency)
        font.bold: true
        font.pixelSize: Theme.fontSizeLarge
        horizontalAlignment: Text.AlignHCenter
    }
    Label {
        visible: !bitcoinprice.error
        anchors.top: currencyLabel.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width - Theme.paddingSmall*2
        wrapMode: Text.Wrap
        text: bitcoinprice.refreshing ? qsTr("Updating...") : bitcoinprice.statusText
        color: bitcoinprice.refreshing ? Theme.primaryColor : Theme.secondaryColor
        font.pixelSize: Theme.fontSizeSmall
        horizontalAlignment: Text.AlignHCenter
    }
    Label {
        visible: bitcoinprice.error;
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: currencyLabel.bottom
        width: parent.width - Theme.paddingSmall*2
        horizontalAlignment: Text.AlignHCenter
        text: bitcoinprice.error
        wrapMode: Text.Wrap
        font.pixelSize: Theme.fontSizeMedium
        color: Theme.highlightColor
    }


    Image {
            id: img
            source: "qrc:/img/coverbackground.png"
            fillMode: Image.PreserveAspectCrop
            anchors.fill: parent
            z: -1
            opacity: Theme.highlightBackgroundOpacity
        }



    CoverActionList {
        id: coverAction
        CoverAction {
            iconSource: "image://theme/icon-cover-refresh"
            onTriggered: bitcoinprice.refresh()
        }
    }

    onStatusChanged: {
         if (status===Cover.Active && !bitcoinprice.error && bitcoinprice.priceLast < 0) {
            bitcoinprice.pushShortcut();
            bitcoinprice.refresh();
        }
    }
}


