import QtQuick 2.0
import Sailfish.Silica 1.0

Column {
    id: root
    property string text
    property real price
    property string currency
    property bool loading
    Label {
        text: root.text
        font.pixelSize: Theme.fontSizeSmall
        color: Theme.secondaryColor
    }
    Label {
        text: formatPrice();
        font.pixelSize: Theme.fontSizeLarge
        color: price < 0 ? Theme.secondaryColor : Theme.primaryColor

        function formatPrice() {
            if (price < 0) {
                return loading ? qsTr("Loading") : qsTr("Not available");
            }
            else {
                return qsTr("%L1 %2").arg(price.toFixed(2)).arg(currency)
            }
        }
    }


}
