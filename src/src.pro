TEMPLATE=app
# The name of your app binary (and it's better if you think it is the whole app name as it's referred to many times)
# Must start with "harbour-"
TARGET = harbour-bitcoinprice

# In the bright future this config line will do a lot of stuff to you
#CONFIG += sailfishapp

# Start of temporary fix for the icon for the Nov 2013 harbour requirements, basically reimplements
# what CONFIG += sailfishapp is supposed to do manually (with small corrections)
# QML files and folders
QT += quick qml
CONFIG += link_pkgconfig
PKGCONFIG += sailfishapp

INCLUDEPATH += /usr/include/sailfishapp

TARGETPATH = /usr/bin
target.path = $$TARGETPATH

DEPLOYMENT_PATH = /usr/share/$$TARGET
qml.files = qml
qml.path = $$DEPLOYMENT_PATH

desktop.files = harbour-bitcoinprice.desktop
desktop.path = /usr/share/applications

icon.files = harbour-bitcoinprice.png
icon.path = /usr/share/icons/hicolor/86x86/apps

INSTALLS += target icon desktop  qml
# End of Nov 2013 fix

SOURCES += src/bitcoinprice.cpp \
    src/elapsedcounter.cpp \
    src/blockchainpriceservice.cpp \
    src/networkpriceservice.cpp \
    src/multisourcepriceservice.cpp \
    src/krakenpriceservice.cpp \
    src/mtgoxpriceservice.cpp

HEADERS += \
    src/elapsedcounter.h \
    src/price.h \
    src/priceservice.h \
    src/blockchainpriceservice.h \
    src/networkpriceservice.h \
    src/multisourcepriceservice.h \
    src/staticpriceservice.h \
    src/krakenpriceservice.h \
    src/mtgoxpriceservice.h

RESOURCES += \
    resources.qrc

OTHER_FILES = \
# You DO NOT want .yaml be listed here as Qt Creator's editor is completely not ready for multi package .yaml's
#
# Also Qt Creator as of Nov 2013 will anyway try to rewrite your .yaml whenever you change your .pro
# Well, you will just have to restore .yaml from version control again and again unless you figure out
# how to kill this particular Creator's plugin
#    ../rpm/harbour-bitcoinprice.yaml \
    ../rpm/harbour-bitcoinprice.spec \
    qml/bitcoinprice.qml \
    qml/cover/CoverPage.qml \
    qml/pages/FirstPage.qml \
    qml/pages/OptionsPage.qml \
    qml/components/PriceLabel.qml


INCLUDEPATH += $$PWD
