Bitcoin Price for Sailfish
==========================

A simple app for Sailfish (Jolla) that shows the price of a bitcoin.

Used [this helloworld template](https://github.com/amarchen/helloworld-pro-sailfish/) as
a project base. Removed tests for now because couldn't get them to
work...


### License

Licensed with MIT license.
